// my-component.js

export default {
  data() {
    return { 
      clickCounter: 0,
      enableOnButton: true,
      enableOffButton: true,
      offWaitSeconds: 5000,
      onWaitSeconds: 5000,
      msg: '',
      apiUrl: 'https://mcd5j7pnjymj6szljbfqhdb6jx2y.pub.sfmc-content.com/g30gzatjisj',
      apiBodyON: {'action': 'on'}, 
      apiBodyOFF: {'action': 'off'},
      apiBodyStatus: {'action': 'status'},
      supListCount: {
        email: 0,
        mobile: 0,
        ad: 0
      },
      resp: {},
    }
  },
  methods: {
    async delay(ms) {  
      await new Promise(res => setTimeout(res, ms));
    },
    async triggerAutomationON(event) {
      // console.log('Trigger Automation')
      this.msg += "<br>----ON Request Started----"
      this.enableOnButton = false;
      this.enableOffButton = false;
      
      let body, currentStage = 'New', isEmailUpdateCompleted = false, isMobileUpdateCompleted = false, isAdUpdateCompleted = false;

      do{
        this.clickCounter++;
        // this.enableOnButton = false;
        // this.enableOffButton = true;
        // console.log('Status check #' + this.clickCounter)

        if(currentStage == 'New') {
          body = this.apiBodyON;
          // console.log('Body is new: ', body);
        } else {
          body = this.apiBodyStatus;
          // console.log('Body changed: ', body);
        }

        const vueThis = this;
        axios
        .post(this.apiUrl, body )
        .then(
          function(response) {
            //this.msg = response;
            if(response.hasOwnProperty('data')) {
              if(response['data'].hasOwnProperty('automationStatus') ){
                currentStage = response['data']['automationStatus']
              }
            }
            // console.log(currentStage, response)
            // console.log(response['data']['automationStatus'])
            //this.resp = {}
            vueThis.resp = response['data'];

            //alert(response.statusText)
            if(response.statusText == 'OK') {
              if(response['data']['automationStatus'] == 'Started') {
                vueThis.msg += "<br>" + "Automation Triggered and started!!"
              }
              else if(response['data']['automationStatus'] == 'InProgress' ) {
                if(response['data'].hasOwnProperty('Count')) {
                  vueThis.supListCount.email = response['data']['Count']['Email'];
                  vueThis.supListCount.mobile = response['data']['Count']['Mobile'];
                  vueThis.supListCount.ad = response['data']['Count']['Ad'];
                } 
                
                vueThis.msg += "<br> Running!" 
                if(vueThis.supListCount.email > 0 & isEmailUpdateCompleted == false) {
                  isEmailUpdateCompleted = true;
                  vueThis.msg += " Adding Records to Email Suppression List is complete."
                }

                if(vueThis.supListCount.mobile > 0 & isMobileUpdateCompleted == false) {
                  isMobileUpdateCompleted = true;
                  vueThis.msg += " Adding Records to Mobile Suppression List is complete."
                }

                if(vueThis.supListCount.ad > 0 & isAdUpdateCompleted == false) {
                  isAdUpdateCompleted = true;
                  vueThis.msg += " Adding Records to Ad Suppression List is complete."
                }


              } else if(response['data']['automationStatus'] == 'Ready'){
                // console.log('Automation status is ready.. Possibly, run complete.')
                vueThis.supListCount.email = response['data']['Count']['Email'];
                vueThis.supListCount.mobile = response['data']['Count']['Mobile'];
                vueThis.supListCount.ad = response['data']['Count']['Ad'];
              }
            }

          }); 

        //wait for 5 seconds        
        await this.delay(vueThis.onWaitSeconds);

      }while(this.supListCount.email == 0 || this.supListCount.mobile == 0 || this.supListCount.ad == 0 ) ;

      //Check if all status messages are posted
      if(this.supListCount.email > 0 & isEmailUpdateCompleted == false) {
        isEmailUpdateCompleted = true;
        this.msg += "Adding Records to Email Suppression List is complete."
      }

      if(this.supListCount.mobile > 0 & isMobileUpdateCompleted == false) {
        isMobileUpdateCompleted = true;
        this.msg += "Adding Records to Mobile Suppression List is complete."
      }

      if(this.supListCount.ad > 0 & isAdUpdateCompleted == false) {
        isAdUpdateCompleted = true;
        this.msg += "Adding Records to Ad Suppression List is complete."
      }

      this.msg += "<br>----ON Request Completed---<br>" 
      this.currentStage = 0;
      
      this.enableOnButton = false;
      this.enableOffButton = true;
    },

    async triggerAutomationOff(event) {
      // console.log('Trigger Automation - OFF')
      this.msg += "<br>----OFF Request Started----"
      this.enableOnButton = false;
      this.enableOffButton = false;

      let body, resp, currentStage = 'New'

      //while(this.clickCounter < 10) {
      do {
        this.clickCounter++;
        //this.enableOnButton = true;
        // this.enableOffButton = false;
        // console.log('Status check #' + this.clickCounter)

        if(currentStage == 'New') {
          body = this.apiBodyOFF;
          // console.log('Body is new: ', body);
        } else {
          body = this.apiBodyStatus;
          // console.log('Body changed: ', body);
        }

        const vueThis = this; 
        axios
          .post(this.apiUrl, body )
          .then(
            function(response) {
              //this.msg = response;
              if(response.hasOwnProperty('data')) {
                if(response['data'].hasOwnProperty('ClearStatus') ){
                  currentStage = response['data']['ClearStatus']
                }
              }
              // console.log(currentStage, response)
              // console.log(response['data']['ClearStatus'])
              //this.resp = {}
              vueThis.resp = response['data'];

              //alert(response.statusText)
              if(response.statusText == 'OK') {
                vueThis.supListCount.email = response['data']['Count']['Email'];
                vueThis.supListCount.mobile = response['data']['Count']['Mobile'];
                vueThis.supListCount.ad = response['data']['Count']['Ad'];
                vueThis.msg += "<br>" + `Count in Email Suppression List ${vueThis.supListCount.email}`
                            +"<br>" +`Count in Mobile Suppression List ${vueThis.supListCount.mobile}`
                            +"<br>" +`Count in Ad Suppression List ${vueThis.supListCount.ad}`
                            +"<br>";
              }

            }); 
        //wait for 5 seconds        
        await this.delay(vueThis.offWaitSeconds);

        this.enableOnButton = true;
        this.enableOffButton = false;

      } while(this.supListCount.email > 0 || this.supListCount.mobile > 0 || this.supListCount.ad > 0 ) ;

      this.msg += "<br>----Off Request Completed---<br>" 
      this.currentStage = 0;
    }
  },
  mounted () {
    // TODO: Get status and set valuesasync triggerAutomationON(event) {
    // console.log('Mouted - getting status')
    let currentStage;
    let body = this.apiBodyStatus;
    const vueThis = this;
    axios
    .post(this.apiUrl, body )
    .then(
      function(response) {
        //this.msg = response;
        if(response.hasOwnProperty('data')) {
          if(response['data'].hasOwnProperty('automationStatus') ){
            currentStage = response['data']['automationStatus']
          }
        }
        // console.log(currentStage, response)
        // console.log(response['data']['ClearStatus'])
        //this.resp = {}
        vueThis.resp = response['data'];

        //alert(response.statusText)
        if(response.statusText == 'OK') {
          vueThis.supListCount.email = response['data']['Count']['Email'];
          vueThis.supListCount.mobile = response['data']['Count']['Mobile'];
          vueThis.supListCount.ad = response['data']['Count']['Ad'];

          /*if(response['data']['automationStatus'] == 'Ready') {
            vueThis.msg += "<br>" + "Automation Ready and not running!!"
          }
          else if(response['data']['automationStatus'] == 'InProgress') {
            vueThis.supListCount.email = response['data']['Count']['Email'];
            vueThis.supListCount.mobile = response['data']['Count']['Mobile'];
            vueThis.supListCount.ad = response['data']['Count']['Ad'];
            vueThis.msg += "<br>" + `Count in Email Suppression List ${vueThis.supListCount.email}`
                        +"<br>" +`Count in Mobile Suppression List ${vueThis.supListCount.mobile}`
                        +"<br>" +`Count in Ad Suppression List ${vueThis.supListCount.ad}`
                        +"<br>";
        } */
      }

      //Enable/ Disbale button set
      if(vueThis.supListCount.email == 0 
          && vueThis.supListCount.mobile == 0
          && vueThis.supListCount.ad == 0) {
            vueThis.enableOffButton = false;
            vueThis.enableOnButton = true;
      }else {
        vueThis.enableOffButton = true;
        vueThis.enableOnButton = false;
      }

      }); 

      
  }, //mounted ends 
  template: `
  <!-- <v-container
    class="px-0"
    fluid
  >
    <v-switch
      v-model="clickCounter"
      :label="clickCounter"
    ></v-switch>
  </v-container> -->
  <h4>Current Count:</h4>
    <div style="background-color:#F7F8FB;margin:11px 0 4px; border: 1px solid #E3E7EF; border-radius: 4px; padding: 10px" >
    Email Suppression List: {{this.supListCount.email}}
    <br>Mobile Suppression List: {{this.supListCount.mobile}}
    <br>Ad Studio Suppression List: {{this.supListCount.ad}}
    </div>
    <br>
    <button id="btnOn" v-bind:disabled="!enableOnButton" @click="triggerAutomationON">
      Enable
    </button>&nbsp;&nbsp;&nbsp;&nbsp;
    <button id="btnOff" v-bind:disabled="!enableOffButton" @click="triggerAutomationOff">
      Disable
    </button>
    <br>
    <h4>Log:</h4>
    <div style="min-height:200px; background-color:#F7F8FB;margin:11px 0 4px; border: 1px solid #E3E7EF; border-radius: 4px" 
    v-html=msg >
    </div>`

}
